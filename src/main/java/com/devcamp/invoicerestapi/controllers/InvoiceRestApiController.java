package com.devcamp.invoicerestapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.invoicerestapi.InvoiceItem;

@RestController
@RequestMapping
@CrossOrigin

public class InvoiceRestApiController {
    @GetMapping("/invoices")
    public ArrayList<InvoiceItem> getListInvoiceItem(){
        //khởi tạo 3 đối tượng hóa đơn có tham số
        InvoiceItem invoice1 = new InvoiceItem("Inv01", "Product1", 10, 10000);
        InvoiceItem invoice2 = new InvoiceItem("Inv02", "Product2", 15, 20000);
        InvoiceItem invoice3 = new InvoiceItem("Inv03", "Product3", 20, 30000);
        //in ra console
        System.out.println(invoice1);
        System.out.println(invoice2);
        System.out.println(invoice3);
        //task 5: tạo arrayList
        ArrayList<InvoiceItem> invoiceItems = new ArrayList<>();
        invoiceItems.add(invoice1);
        invoiceItems.add(invoice2);
        invoiceItems.add(invoice3);
        return invoiceItems;
    }
}
